-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2022 at 04:38 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_magang`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_absen`
--

CREATE TABLE `data_absen` (
  `no` int(11) NOT NULL,
  `id_absen` int(10) NOT NULL,
  `tanggal_absen` date NOT NULL,
  `masuk` time NOT NULL,
  `keluar` time NOT NULL,
  `keterangan` enum('Hadir','Izin','Sakit') NOT NULL,
  `status` varchar(10) DEFAULT '0',
  `status2` varchar(10) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_absen`
--

INSERT INTO `data_absen` (`no`, `id_absen`, `tanggal_absen`, `masuk`, `keluar`, `keterangan`, `status`, `status2`) VALUES
(1, 2, '2022-12-02', '09:04:00', '00:00:00', 'Hadir', '1', '1'),
(2, 2, '2022-12-01', '09:07:00', '10:06:00', 'Hadir', '1', '1'),
(3, 1, '2022-12-03', '09:05:00', '00:00:00', 'Hadir', '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `data_admin`
--

CREATE TABLE `data_admin` (
  `id_admin` int(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `telepon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_admin`
--

INSERT INTO `data_admin` (`id_admin`, `nama`, `telepon`) VALUES
(1, 'Admin', '012');

-- --------------------------------------------------------

--
-- Table structure for table `data_dosenpembimbing`
--

CREATE TABLE `data_dosenpembimbing` (
  `id_dosen` int(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `telepon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_dosenpembimbing`
--

INSERT INTO `data_dosenpembimbing` (`id_dosen`, `nama`, `telepon`) VALUES
(1, 'Jaya', '789');

-- --------------------------------------------------------

--
-- Table structure for table `data_jurnal`
--

CREATE TABLE `data_jurnal` (
  `id_jurnal` int(10) NOT NULL,
  `id_absen` int(10) NOT NULL,
  `tanggal_jurnal` date NOT NULL,
  `jurnal` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_jurnal`
--

INSERT INTO `data_jurnal` (`id_jurnal`, `id_absen`, `tanggal_jurnal`, `jurnal`) VALUES
(1, 3, '2022-11-28', 'bismillah amin '),
(4, 1, '2022-11-28', 'test tampilan test tampilan test tampilan test tampilan test tampilan test tampilan test tampilan test tampilan test tampilan test tampilan test tampilan test tampilan test tampilan test tampilan test tampilan test tampilan test tampilan '),
(5, 4, '2022-11-28', 'asolali asolali euy euy'),
(7, 3, '2022-11-29', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `data_magang`
--

CREATE TABLE `data_magang` (
  `id_magang` int(11) NOT NULL,
  `id_absen` int(10) NOT NULL,
  `id_perusahaan` varchar(10) NOT NULL,
  `id_jurnal` varchar(10) DEFAULT NULL,
  `mulai_magang` date DEFAULT NULL,
  `berakhir_magang` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `data_mahasiswa`
--

CREATE TABLE `data_mahasiswa` (
  `id_absen` int(10) NOT NULL,
  `nama_mahasiswa` varchar(255) NOT NULL,
  `jurusan` varchar(50) NOT NULL,
  `angkatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_mahasiswa`
--

INSERT INTO `data_mahasiswa` (`id_absen`, `nama_mahasiswa`, `jurusan`, `angkatan`) VALUES
(1, 'Asep', 'RPL', '2019'),
(2, 'Bagas', 'TKJ', '2019'),
(3, 'Cahyo', 'RPL', '2019'),
(4, 'Dadang', 'TKJ', '2019'),
(5, 'Evi', 'RPL', '2019'),
(6, 'Fahri', 'TKJ', '2019'),
(7, 'Gilang', 'TKJ', '2019'),
(8, 'Hikmal', 'RPL', '2019'),
(9, 'Raditya', 'RPL', '2019'),
(10, 'Utun', 'TKJ', '2019');

-- --------------------------------------------------------

--
-- Table structure for table `data_pembimbinglapangan`
--

CREATE TABLE `data_pembimbinglapangan` (
  `id_pembimbing` int(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `perusahaan` varchar(255) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `telepon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_pembimbinglapangan`
--

INSERT INTO `data_pembimbinglapangan` (`id_pembimbing`, `nama`, `perusahaan`, `jabatan`, `telepon`) VALUES
(1, 'Ican', 'SNI', 'Direktur', '123'),
(2, 'Ardhien', 'SNI', 'Operational Manager', '456');

-- --------------------------------------------------------

--
-- Table structure for table `data_perusahaan`
--

CREATE TABLE `data_perusahaan` (
  `id_perusahaan` varchar(10) NOT NULL,
  `nama_perusahaan` varchar(255) NOT NULL,
  `alamat_perusahaan` text NOT NULL,
  `telepon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `data_users`
--

CREATE TABLE `data_users` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_users`
--

INSERT INTO `data_users` (`id_user`, `nama`, `username`, `password`, `level`) VALUES
(1, 'Admin', 'admin', 'admin', 'admin'),
(2, 'Ican', 'ican', 'ican', 'pembimbing'),
(3, 'Jaya', 'jaya', 'jaya', 'dosen'),
(4, 'Asep', 'asep', 'asep', 'mahasiswa');

-- --------------------------------------------------------

--
-- Table structure for table `upload`
--

CREATE TABLE `upload` (
  `id_file` int(11) NOT NULL,
  `nama_file` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `upload`
--

INSERT INTO `upload` (`id_file`, `nama_file`) VALUES
(1, 'Screenshot (1).png'),
(2, 'Screenshot (6).png'),
(3, 'Screenshot (20).png'),
(4, 'Screenshot (11).png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_absen`
--
ALTER TABLE `data_absen`
  ADD UNIQUE KEY `no` (`no`),
  ADD KEY `id_absen` (`id_absen`);

--
-- Indexes for table `data_admin`
--
ALTER TABLE `data_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `data_dosenpembimbing`
--
ALTER TABLE `data_dosenpembimbing`
  ADD PRIMARY KEY (`id_dosen`);

--
-- Indexes for table `data_jurnal`
--
ALTER TABLE `data_jurnal`
  ADD PRIMARY KEY (`id_jurnal`),
  ADD KEY `id_absen` (`id_absen`);

--
-- Indexes for table `data_magang`
--
ALTER TABLE `data_magang`
  ADD PRIMARY KEY (`id_magang`),
  ADD KEY `id_absen` (`id_absen`),
  ADD KEY `id_perusahaan` (`id_perusahaan`),
  ADD KEY `id_jurnal` (`id_jurnal`);

--
-- Indexes for table `data_mahasiswa`
--
ALTER TABLE `data_mahasiswa`
  ADD PRIMARY KEY (`id_absen`);

--
-- Indexes for table `data_pembimbinglapangan`
--
ALTER TABLE `data_pembimbinglapangan`
  ADD PRIMARY KEY (`id_pembimbing`);

--
-- Indexes for table `data_perusahaan`
--
ALTER TABLE `data_perusahaan`
  ADD PRIMARY KEY (`id_perusahaan`);

--
-- Indexes for table `data_users`
--
ALTER TABLE `data_users`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `upload`
--
ALTER TABLE `upload`
  ADD UNIQUE KEY `id_file` (`id_file`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_absen`
--
ALTER TABLE `data_absen`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_jurnal`
--
ALTER TABLE `data_jurnal`
  MODIFY `id_jurnal` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `data_magang`
--
ALTER TABLE `data_magang`
  MODIFY `id_magang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_users`
--
ALTER TABLE `data_users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `upload`
--
ALTER TABLE `upload`
  MODIFY `id_file` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_absen`
--
ALTER TABLE `data_absen`
  ADD CONSTRAINT `data_absen_ibfk_1` FOREIGN KEY (`id_absen`) REFERENCES `data_mahasiswa` (`id_absen`);

--
-- Constraints for table `data_jurnal`
--
ALTER TABLE `data_jurnal`
  ADD CONSTRAINT `data_jurnal_ibfk_1` FOREIGN KEY (`id_absen`) REFERENCES `data_mahasiswa` (`id_absen`);

--
-- Constraints for table `data_magang`
--
ALTER TABLE `data_magang`
  ADD CONSTRAINT `data_magang_ibfk_2` FOREIGN KEY (`id_perusahaan`) REFERENCES `data_perusahaan` (`id_perusahaan`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
